from setuptools import setup

setup(
    name='nmapwrapper',
    version='2.8.3',
    description=
    'Wrapper of NMAP native Linux command...Listens remote requests for local network',
    long_description='Not given yet !',
    # Author details
    author="Miltos K. Vimplis",
    author_email="mvimblis@gmail.com",
    url='https://gitlab.com/recipes-desire/python-iot/python3-nmapwrapper',
    license='MIT License',
    packages=['nmapwrapper'],
    #package_data=[],
    #entry_points={
    #    'console_scripts': [
    #        'nmapwrapper=nmapwrapper.nmap_wrapper:main',
    #    ]
    #},
    #install_requires=['tcpclientserver==1.0.0'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.7',
        'Topic :: System :: Network :: Monitoring',
    ],
)

# install_requires versions are handled by Python in runtime only!
# if you update this also update the BitBake recipe of the app using this package
# something like RDEPENDS_${PN} += "python3-tcpclientserver (= 1.0.0)"
