
from .discover_active_hosts import open_hosts_now
from .nmap_version import nmap_version

CORRECT_TARGETS = ["host", "port", "ping"]
CORRECT_ACTIONS = ["discovery", "scan", "pong"]


class NMAPActions(object):
    def __init__(self, logger, target, action, cidr=None, server=None):
        self.logger = logger
        self.target = target.lower()
        self.action = action.lower()
        self.server = server
        self.cidr = cidr

    def check_target(self):
        if self.target not in CORRECT_TARGETS:
            raise Exception("Requested Target Not Supported ... {}".\
                            format(self.target))

    def check_action(self):
        if self.action not in CORRECT_ACTIONS:
            raise Exception("Requested Action Not Supported ... {}".\
                            format(self.action))

    def nmap_open_ports(self):
        from .scan_open_ports import listening_ports_for_server
        listening_ports = listening_ports_for_server(self.logger, self.server)

        return listening_ports

    def nmap_host_discovery(self):
        self.logger.debug("Inside NMAP_HOST_DISCOVERY !")
        hosts = open_hosts_now(self.logger, self.cidr)

        return hosts

    def nmap_version(self):
        self.logger.debug("Inside NMAP-VERSION !")
        version_str = nmap_version(self.logger)

        return version_str

    def call_nmap_target_action(self):
        self.check_target()
        self.check_action()

        if self.target == "host" and self.action == "discovery":
            return self.nmap_host_discovery()
        elif self.target == "port" and self.action == "scan" and self.server is not None:
            return self.nmap_open_ports()
        elif self.target == "ping" and self.action == "pong":
            return self.nmap_version()
        else:
            raise Exception("No Rule for Target {} && Action {} && Server {}".\
                            format(self.target, self.action, self.server))
