from nmapwrapper.nmap_actions import NMAPActions


class NMapWrapper(object):
    tool = None
    target = None
    action = None

    server = None

    # ##
    # Save New Request for Handling
    def __init__(self, logger, request, cidr):
        self.logger = logger
        self.logger.debug(
            "Inside NMapWrapper ... Ready to Handle New Request \"%s\" !",
            request)
        self.request = request
        self.cidr = cidr

    # ##
    # Parse string and find {request-type, net-object, required-action} ... ex nmap:host:discovery
    def to_parse(self):
        # Check requests beginning
        if not self.request.startswith("nmap:"):
            raise Exception("Unknown Request Type ... Can Handle Only nmap: !")

        # Check request target & action
        total = self.request.split(":")
        if len(total) > 5:
            raise Exception(
                "Uknown Request Format ... Can handle Only nmap:host:discovery"
            )

        self.tool = total[0]
        self.target = total[1]
        self.action = total[2]

        if len(total) > 3:
            self.server = total[3]

        # ends with !
        if self.action.endswith("!"):
            # Remove exclamation mark
            self.action = self.action[:-1]

        # if self.key.endswith("port:scan!"):
        #    logger.debug("Have a PORT-SCAN request for NMAP")
        #    self.nmap_open_ports()

    # ##
    # Call right tool for requested target & action
    def call_right_tool(self):
        if self.tool.lower() != "nmap":
            raise Exception("Call Right Tool Error ... ABORTING !")

        # This Agent is only responsible for NMAP network-tool
        #logger.debug("++++++ target = %s , %s, %s", self.target, self.action, self.server)

        if self.target.lower() == "host":
            return NMAPActions(self.logger,
                               cidr=self.cidr,
                               target=self.target,
                               action=self.action)
        elif self.target.lower() == "port":
            return NMAPActions(self.logger,
                               target=self.target,
                               action=self.action,
                               server=self.server)
        elif self.target.lower() == "ping" and self.action.lower() == "pong":
            return NMAPActions(self.logger, target="ping", action="pong")
        else:
            raise NotImplementedError("Do not know how to handle target ... {}".\
                                      format(self.target))

    # ##
    # Handle New Request
    def handle_new_request(self):
        # Step 1 --> Parse the long-string of request from client to server
        self.to_parse()

        # Step 2 --> Prepare appropriate tool for target & action
        tool = self.call_right_tool()

        # Step 3 --> Call selected tool to execute requested action on specified target object
        targets_found_str = tool.call_nmap_target_action()

        #logger.debug("Targets Found by NMAP ... %s", targets_found_str)

        return targets_found_str


def main():
    '''def main():
    file_conf = ExtConfig("/etc/nmapwrapper/nmapwrapper.conf")
    file_conf.read_config()
    default_section = file_conf.get_default_section()

    log_filename = default_section["log-filename"]
    log_level = default_section["log-level"]
    msg_format = default_section["msg-format"]
    max_bytes = default_section["max-bytes"]
    backup_count = default_section["backup-count"]

    log_setup(filename=log_filename,
              level=log_level,
              msg_format=msg_format,
              maxBytes=max_bytes,
              backupCount=backup_count)

    logger = get_logger()

    logger.debug("NMap-Wrapper Ready to Start !")

    is_daemon = default_section["is-daemon"]

    agent_host = default_section["agent-host"]
    listening_port = default_section["listening-port"]

    whitelist_remote_requests = default_section["whitelist-remote-requests"]
    whitelist_arr = whitelist_remote_requests.split(",")
    logger.debug("Whitelist for remote requests is +++ %s", whitelist_arr)

    if is_daemon:
        # Run the NMAP-Wrapper as a Linux Daemon
        pid = default_section["pid"]

        logger.debug(
            "\n" +
            "********************************************************************\n"
            +
            "Ready to Run the NMAP-Wrapper Application as a Linux Daemon with PID ... %s !\n"
            +
            "********************************************************************",
            pid)
    else:
        logger.debug(
            "Ready to Run the NMAP-Wrapper as a Standalone Application !")
        prepare_agent_for_remote_requests(logger,
                                          host=agent_host,
                                          port=listening_port,
                                          whitelist=whitelist_arr)
    '''
    pass
