import datetime

from .constants import get_nmap_version_command
from .nmap_command import NMapCommandInSubprocess


IMPORTANT_LINE = "Host:"


def nmap_version(logger):
    command_base = get_nmap_version_command()
    command_all = command_base

    # Run Linux Command in Subprocess
    start = datetime.datetime.now()
    logger.debug("***** Ready to Send Torpedo to Target Board ... %s @ %s !!!",
                 command_all, start)

    nmap_command = NMapCommandInSubprocess(command_all)
    nmap_command.run_linux_command_blocking()

    # NMAP Response .. utf-8
    whole_response_string_utf8 = str(nmap_command)

    return whole_response_string_utf8

    # Return NMAP Response ... Total
    #return whole_response_string_utf8
