def get_host_discovery_command():
    #command = os.environ["HOST-DISCOVERY-COMMAND"]
    command = "nmap -v -sn"

    # Return nmap with necessary parameters
    return command


def get_port_scanning_command():
    '''
    Port Scanning
    '''

    #command = os.environ["PORT-SCANNING-COMMAND"]
    command = "nmap -sT --top-ports 1000 -v -oG - "

    #SCAN_TCP = "nmap -sT --top-ports 1000 -v -oG - "
    #SCAN_UDP = "sudo nmap -sU --top-ports 1000 -v -oG - "
    #SCAN_TCP_UDP = "sudo nmap -sU -sT "

    # Return the appropriate form of nmap for open port-scanning
    return command


def get_nmap_version_command():
    '''
    NMAP Version
    '''
    command = "nmap --version"
    return command


def get_all(cidr):
    nmap_version_cmd = get_nmap_version_command()

    host_discovery_cmd = get_host_discovery_command()

    #HOST_DISCOVERY_CIDR = get_host_discovery_cidr()
    host_discovery_dict = {'command': host_discovery_cmd, 'cidr': cidr}

    port_scanning_cmd = get_port_scanning_command()

    return [nmap_version_cmd, host_discovery_dict, port_scanning_cmd]
