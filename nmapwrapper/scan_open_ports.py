#import pysnooper

import datetime

from .constants import get_port_scanning_command
from .nmap_command import NMapCommandInSubprocess


IMPORTANT_LINE = "Host:"


#@pysnooper.snoop()
def listening_ports_for_server(logger, hot_server):
    logger.debug("Port Scanning Request for Server ... %s", hot_server)

    command_base = get_port_scanning_command()

    # Add into the command which server is investigated for open ports
    command_all = command_base + " " + hot_server

    # Run Linux Command in Subprocess
    start = datetime.datetime.now()
    logger.debug(
        "***** Ready to Send PortsReq to Target Board ... %s @ %s !!!",
        command_all, start)

    nmap_command = NMapCommandInSubprocess(command_all)
    nmap_command.run_linux_command_blocking()

    #logger.debug("****** NMap Port Scan Response ******\n%s", nmap_command)

    # Process command output and prepare list of open ports
    rlines = nmap_command.get_list_of_answers()

    # First element is Examined Server
    ports_open_arr = [hot_server]
    for line in rlines:
        #logger.debug("+++++++++++++++ %s", line)

        if line.strip().startswith(
                IMPORTANT_LINE) and "Ports:" in line and "open" in line:
            # ****************
            #  Found Open Port
            # ****************
            #logger.debug("** %s", line)

            ports_open_arr.append(line)

    # FInished Scanning Open Ports at Server
    logger.debug("Port Scanning Elapsed Time ... %s *****",
                 datetime.datetime.now() - start)

    return ports_open_arr
