import datetime


from .constants import get_all
from .nmap_command import NMapCommandInSubprocess



def open_hosts_now(logger, cidr):
    dict_io = get_all(cidr)[1]
    command_base = dict_io["command"]
    network_cidr = dict_io['cidr']

    command_str = command_base + " " + network_cidr

    # Run Linux Command in Subprocess
    start = datetime.datetime.now()
    logger.debug("***** Ready to Send Torpedo to Target Board ... %s @ %s !!!",
                 command_str, start)

    nmap_command = NMapCommandInSubprocess(command_str)
    nmap_command.run_linux_command_blocking()

    # NMAP Response .. utf-8

    # Process command output and prepare list of active servers
    rlines = nmap_command.get_list_of_answers()

    hostsup_list = []
    for i in range(0, len(rlines)):
        #logger.debug("+++++++++++++++ %s", rlines[i])
        if rlines[i].strip().startswith('Host is up'):
            # ***************
            #  Found Host Up
            # ***************
            #logger.debug("** #%d: \n\t %s \n\t %s", i, rlines[i], rlines[i-1])

            hostsup_list.append(rlines[i - 1])

    # How many Hosts-Up found ?
    logger.debug("Number of Hosts-Up Found = %d", len(hostsup_list))
    logger.debug("Elapsed Time ... %s *****", datetime.datetime.now() - start)

    # Get a list of kines 'Host is Up'
    return hostsup_list

    # Return NMAP Response ... Total
    #return whole_response_string_utf8
